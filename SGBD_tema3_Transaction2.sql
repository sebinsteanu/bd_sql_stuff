--transaction 2


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED				--dirty reads
SET TRANSACTION ISOLATION LEVEL READ COMMITTED					--solution: BLOCARE SHARED PE OPERATIE DE CITIRE

BEGIN TRAN
	SELECT * FROM Senator WHERE nume = 'Vasile Blaga'
	WAITFOR DELAY '00:00:15'
	SELECT * FROM Senator WHERE nume = 'Vasile Blaga'			--citesc inainte de terminarea tranzactiei
COMMIT TRAN
GO



SET TRANSACTION ISOLATION LEVEL READ COMMITTED					--non repeatable reads
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ					--solution: BLOCARE SHARED PE TRANZACTIE DE CITIRE

BEGIN TRAN
	SELECT * FROM Senator
	WAITFOR DELAY '00:00:05'
	SELECT * FROM Senator										--citesc dupa terminarea tranzactiei
COMMIT TRAN
GO


SET TRANSACTION ISOLATION LEVEL REPEATABLE READ					--phantom reads
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE					--SOLUTION: LOCK KEY RANGE

BEGIN TRAN
	SELECT * FROM Senator
	WHERE varsta BETWEEN 50 AND 70
	WAITFOR DELAY '00:00:05'
	SELECT * FROM Senator
	WHERE varsta BETWEEN 50 AND 70
COMMIT TRAN
go

SET DEADLOCK_PRIORITY HIGH							--MANAGE THE CHOSEN VICTIM
go

CREATE PROCEDURE deadlock1
AS
BEGIN TRAN											--DEADLOCK
	UPDATE Oras SET nume = nume + 'TR 2' WHERE codo = 3
	WAITFOR DELAY '00:00:10'
	UPDATE Senator SET nume = nume + 'TR 2' WHERE codm = 1
COMMIT TRAN
GO





----------------------

use practic_SGBD;
go


begin tran
update Utilizatori 
set oras = 'orjksadfsadfnkjns'
where codU = 1
commit tran

