--transaction 1

use Romania;
GO


BEGIN TRAN											--dirty reads
	UPDATE Senator SET varsta = 160
		WHERE nume = 'Vasile Blaga'
	WAITFOR DELAY '00:00:10'
	ROLLBACK TRANSACTION							
GO


select * from Senator


BEGIN TRAN											--non repeatable reads
	WAITFOR DELAY '00:00:05'
	UPDATE Senator SET varsta = 130
		WHERE nume = 'Valeriu-Victor Boeriu'
COMMIT TRAN
GO



BEGIN TRAN											--phantom reads
	--WAITFOR DELAY '00:00:04'
	INSERT INTO Senator VALUES('Ionica Mitica', 60)
COMMIT TRAN
GO


SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
go

CREATE PROCEDURE deadlock2
AS
BEGIN TRAN											--DEADLOCK
	UPDATE Senator SET nume = nume + 'TR 1' WHERE codm = 1
	WAITFOR DELAY '00:00:10'
	UPDATE Oras SET nume = nume + 'TR 1' WHERE codo = 3
COMMIT TRAN
GO

SELECT * FROM Senator
SELECT * FROM Oras






----------------------------------------------------------------------------


use practic_SGBD;
go

set TRANSACTION isolation level read committed;

set TRANSACTION isolation level REPEATABLE read;


begin tran
SELECT * FROM Utilizatori
WHERE codU = 1
WAITFOR DELAY '00:00:03'
SELECT * FROM Utilizatori
WHERE codU = 1
Commit tran


