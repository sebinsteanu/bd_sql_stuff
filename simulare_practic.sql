/*
Ve?i gestiona Ziua Izabelei din 14 martie 2017 cu ajutorul unei baze de date.
Entit??ile de interes sunt Invita?i, Corturi, Forma?ii ?i Firme de catering. 
Un invitat
are nume, v�rst? ?i este alocat unui cort. 
Un cort 
are un nume, o capacitate maxim? ?i este aprovizionat de o firm? de catering. 
O firm? de catering 
are un nume, o adres? ?i un flag care indic? dac? ofer? ?i meniu vegetarian. 
O forma?ie 
are un nume, un gen muzical ?i un onorariu / concert; poate concerta �n mai multe corturi, dar o singur? dat? �ntr-un anumit cort. 
�ntr-un cort 
pot sus?ine concerte mai multe forma?ii, pentru fiecare concert specific�ndu-se ora de �ncepere ?i ora de �ncheiere (perechi ora:minut AM/PM).

1. Scrie?i un script SQL care creeaz? un model rela?ional pentru a reprezenta datele.							4p
2. Crea?i o procedur? stocat? care are ca parametri un onorariu O, un moment din zi M ?i o valoare O1. Dac? nu exist? nicio forma?ie al c?rei onorariu s? dep??easc?
O, cresc onorariile tuturor forma?iilor cu O1 ?i se afi?eaz? un mesaj informativ parametrizat (PRINT). Altfel, se elimin? toate concertele forma?iilor al c?ror
onorariu este mai mare dec�t O ?i care se �ncheie �nainte de momentul M.										2p
3. Crea?i un view care afi?eaz? numele corturilor ale c?ror concerte o cost? pe Izabela peste 40.000 de lei.	1p
4. Implementa?i o func?ie care returneaz? toate numele corturilor �n care Izabela a programat cel pu?in C concerte (unde C >= 1) ?i care servesc sau nu meniu 
vegetarian (ultima condi?ie este specificat? de asemenea printr-un parametru; se presupune c? �ntr-un cort aprovizionat de o firm? de catering care ofer? ?i meniu
vegetarian, se consum? ?i un astfel de meniu).																	2p

1p of
*/

use sem_last;

DROP TABLE Concert;
go
DROP TABLE Invitat;
go
DROP TABLE Cort;
go
DROP TABLE Formatie;
go
DROP TABLE Firma;
go
CREATE TABLE Firma(
	codF INT PRIMARY KEY,
	nume VARCHAR(100),
	adresa VARCHAR(100),
	ofera BIT
)
CREATE TABLE Cort(
	codC INT PRIMARY KEY,
	nume VARCHAR(100),
	capacitate INT,
	codF INT FOREIGN KEY REFERENCES Firma(codF)
)
CREATE TABLE Invitat(
	codI INT PRIMARY KEY,
	nume VARCHAR(100),
	varsta INT,
	codC INT FOREIGN KEY REFERENCES Cort(codC)
)
CREATE TABLE Formatie(
	codF INT PRIMARY KEY,
	nume VARCHAR(100),
	gen VARCHAR(100),
	onorariu INT
)

CREATE TABLE Concert(
	codF INT REFERENCES Formatie(codF),
	codC INT REFERENCES Cort(codC),
	oraStart TIME,
	oraEnd TIME,
	PRIMARY KEY (codF,codC)
)



--		2. Crea?i o procedur? stocat? care are ca parametri un onorariu O, un moment din zi M ?i o valoare O1. 
--	Dac? nu exist? nicio forma?ie al c?rei onorariu s? dep??easc?
--	O, cresc onorariile tuturor forma?iilor cu O1 ?i se afi?eaz? un mesaj informativ parametrizat (PRINT). Altfel, se elimin? toate concertele forma?iilor al c?ror
--	onorariu este mai mare dec�t O ?i care se �ncheie �nainte de momentul M.										2p

go
CREATE PROC my_proc @O INT, @moment TIME, @O1 INT
AS

	IF NOT EXISTS (SELECT * FROM Formatie F
					WHERE F.onorariu > @O)
	BEGIN
		UPDATE Formatie
			SET onorariu = onorariu + @O1;
		PRINT ('S-au marit onorariile cu ' + CAST(@o1 AS CHAR(10)));
	END
	ELSE
		DELETE FROM Concert
			WHERE codF IN (SELECT F.codF FROM Formatie F
							WHERE F.onorariu > @O) 
			AND @moment > oraEnd
GO

SELECT * FROM Concert
SELECT * FROM Cort
SELECT * FROM Formatie
SELECT * FROM Firma

INSERT INTO Cort
	VALUES (3,'nume1',10000,5),(2,'doi',40000,5)
INSERT INTO Concert
	VALUES (5,1,'09:00 PM', '11:00 PM')
INSERT INTO Formatie
	VALUES (5,'PM_formatie LOL', 'gen', 60);

INSERT INTO Firma
	VALUES (5,'PM_firma LOL', 'adresa', 1);
go

EXEC my_proc 150,'10:10 PM',10
go
--	3. Crea?i un view care afi?eaz? numele corturilor ale c?ror concerte o cost? pe Izabela peste 40.000 de lei.	1p
CREATE VIEW my_vedere
AS
	SELECT C.nume
	FROM Cort C
	WHERE C.codC IN (SELECT CC.codC
					FROM Concert CC	
					WHERE CC.codC = C.codC
					GROUP BY CC.codC
					HAVING 40000 < (SELECT SUM(FF.onorariu)
									FROM Formatie FF
									WHERE FF.codF = CC.codF)
					)


GO

--		4. Implementa?i o func?ie care returneaz? toate numele corturilor �n care Izabela a programat cel pu?in C concerte (unde C >= 1) ?i care servesc sau nu meniu 
--	vegetarian (ultima condi?ie este specificat? de asemenea printr-un parametru; se presupune c? �ntr-un cort aprovizionat de o firm? de catering care ofer? ?i meniu
--	vegetarian, se consum? ?i un astfel de meniu).																	2p

CREATE FUNCTION my_func (@C INT, @bool BIT)
RETURNS TABLE
AS
	RETURN SELECT C.nume
			FROM Cort C	 INNER JOIN Firma F
						ON F.codF = C.codF
			WHERE C.codC IN (SELECT CC.codC
							FROM Concert CC
							GROUP BY CC.codC
							HAVING @C <= COUNT(CC.codF))
					AND F.ofera = @bool
go

DROP FUNCTION my_func;
SELECT * FROM my_func(1,1);