-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

---------------------------------------2---------------------------------
CREATE PROC recreeazaLegaturi
AS
	ALTER TABLE Detine ADD CONSTRAINT pk_detine
		PRIMARY KEY (codt,codb)
	ALTER TABLE detine ADD CONSTRAINT fk_update_banca
		FOREIGN KEY (codb) REFERENCES Banca(codb)
			ON DELETE CASCADE ON UPDATE CASCADE
	
	ALTER TABLE Membru ADD CONSTRAINT pk_membru
		PRIMARY KEY (cods,codm)
	ALTER TABLE Membru ADD CONSTRAINT fk_update_senat 
		FOREIGN KEY (cods) REFERENCES Senat(cods)
			ON DELETE CASCADE ON UPDATE CASCADE
	
	ALTER TABLE Banca_client ADD CONSTRAINT pk_banca_client
		PRIMARY KEY (codc,codb)
	ALTER TABLE Banca_client ADD CONSTRAINT update_banca 
		FOREIGN KEY (codb) REFERENCES Banca(codb)
			ON DELETE CASCADE ON UPDATE CASCADE
GO
EXEC recreeazaLegaturi;
DROP PROC recreeazaLegaturi;
GO

---------------------------------------------3----------------------------------------

/*scrieti cate o procedura stocata care introduce date intr-un tabel, pentru cel putin 5 tabele, inclusiv un tabel cu o cheie primara compusa; 
-----------parametrii unei astfel de proceduri sunt atributele care descriu entitatile / relatiile din tabel, mai putin coloanele cheilor primare; 
-----------fiecare procedura va utiliza functii pentru validarea anumitor parametri; 
se cer cel putin cinci functii user-defined (optional se pot utiliza, pe langa aceste cinci functii, si functii sistem);
*/

CREATE FUNCTION dbo.isValidEmail(@EMAIL varchar(100))		--verifica daca emailul e valid
RETURNS bit as
BEGIN     
  DECLARE @bitRetVal as Bit
  IF (@EMAIL <> '' AND @EMAIL NOT LIKE '_%@__%.__%')
     SET @bitRetVal = 0  -- Invalid
  ELSE 
    SET @bitRetVal = 1   -- Valid
  RETURN @bitRetVal
END 
GO
CREATE FUNCTION isValidNume(@name VARCHAR(100))				--verifica daca numele contine altceva decat litere
RETURNS BIT
AS
BEGIN
	DECLARE @bitSgn AS BIT;
	IF ( LEN(@name) < 2 OR @name LIKE '%[^a-zA-Z]%')
		SET @bitSgn = 0;
	ELSE 
		SET @bitSgn = 1;
	RETURN @bitSgn;
END
GO
CREATE FUNCTION normalizeazaNume(@nume VARCHAR(100))		--face uppercase la primul caracter  
RETURNS VARCHAR(100) AS
BEGIN
	SET @nume=UPPER(LEFT(@nume,1))+LOWER(SUBSTRING(@nume,2,LEN(@nume)))
	RETURN @nume
END
GO
CREATE FUNCTION isValidVarsta(@varsta INT)					--verifica daca varsta e valida: intre 18 si 300 ani 
RETURNS BIT
AS
BEGIN
	RETURN CASE WHEN @varsta>=18 AND @varsta < 300 THEN 1 ELSE 0 
			END
END
GO
CREATE FUNCTION isValidSite(@site VARCHAR(100))				--verifica daca site-ul e valid 
RETURNS BIT
AS
BEGIN
	DECLARE @bitVal AS BIT
	IF (@site = '' OR @site NOT LIKE '%www.__%.__%')
		SET @bitVal = 0;
	ELSE 
		SET @bitVal = 1;
	RETURN @bitVal;
END
GO
CREATE FUNCTION isValidDate(@data VARCHAR(100))				--valideaza formatul datei calendaristice
RETURNS BIT
AS
BEGIN
	DECLARE @bitVal AS BIT;
	IF ( @data <> '' AND @data NOT LIKE '[0-2][0-9][0-9][0-9]-[1]%[0-2]-[0-3][0-9]' )
		SET @bitVal = 0;
	ELSE 
		SET @bitVal = 1;
	RETURN @bitVal;
END
GO
DROP FUNCTION dbo.isValidDate;
DROP FUNCTION dbo.isValidEmail;
DROP FUNCTION dbo.isValidNume;
DROP FUNCTION dbo.normalizeazaNume;
DROP FUNCTION dbo.isValidVarsta;
DROP FUNCTION dbo.isValidSite;	
GO

--  ISDATE (),  ISNUMERIC (), LEN (), LEFT (), SUBSTRING (),

CREATE PROC adaugaTara(@codt INT, @nume VARCHAR(100), @nr_loc INT)
AS
	IF (ISNUMERIC(@codt) = 1 AND dbo.isValidNume(@nume) = 1 AND ISNUMERIC(@nr_loc) = 1)
		SET @nume = dbo.normalizeazaNume(@nume);
		INSERT INTO Tara VALUES(@codt,@nume,@nr_loc);
DROP PROC adaugaTara;
GO

CREATE PROC adaugaClient(@codC INT, @nume VARCHAR(100), @bani NUMERIC, @varsta INT, @mail VARCHAR(50))
AS
	IF (ISNUMERIC(@codC) = 1 AND dbo.isValidNume(@nume) = 1 AND ISNUMERIC(@bani) = 1 AND dbo.isValidVarsta(@varsta) = 1 AND dbo.isValidEmail(@mail) = 1 )
		SET @nume = dbo.normalizeazaNume(@nume);
		INSERT INTO Client VALUES(@codC,@nume,@bani,@varsta,@mail);
DROP PROC adaugaClient;
GO

CREATE PROC adaugaRelatieTB(@codT INT, @codB INT)
AS
	IF ( ISNUMERIC(@codT) = 1 AND ISNUMERIC(@codB) = 1 )
		INSERT INTO Detine VALUES(@codT, @codB)
GO

CREATE PROCEDURE adaugaConsiliu(@codC INT, @site_cons VARCHAR(100), @nr_membrii INT, @codJ INT)
AS
BEGIN
	IF ( ISNUMERIC(@codC) = 1 AND dbo.isValidSite(@site_cons) = 1 AND ISNUMERIC(@nr_membrii) = 1 AND ISNUMERIC(@codJ) = 1 )
		INSERT INTO Consiliu_Judetean VALUES (@codC, @nr_membrii, @site_cons, @codJ);
END
GO

CREATE PROC adaugaPresedinte(@codP INT, @nume VARCHAR(100), @data_start DATE, @codT INT, @data_end VARCHAR(100))
AS
BEGIN
	IF( ISNUMERIC(@codP) = 1 AND dbo.isValidNume(@nume) = 1 AND  dbo.isValidDate(@data_start) = 1 AND dbo.isValidDate(@data_end) = 1 AND ISNUMERIC(@codT) = 1)
		SET @nume = dbo.normalizeazaNume(@nume);
		INSERT INTO Presedinte VALUES (@codP, @nume, @data_start, @codT, @data_end);
END
DROP PROCEDURE adaugaPresedinte;
GO

--teste proceduri
EXEC adaugaTara 4,'Belgia', 11200000;
EXEC adaugaTara 5,'france', 6603000;
EXEC adaugaTara 6,'Germany',80620000;
EXEC adaugaTara 7,'luxembourg',543000;

EXEC adaugaClient 4,'laura',10000000,29,'laura@gmail.com';
EXEC adaugaClient 5,'mirinda',3550000,29,'mirinda@gmail.com';

EXEC adaugaRelatieTB 4,1;
EXEC adaugaRelatieTB 4,3;
EXEC adaugaRelatieTB 4,6;
EXEC adaugaRelatieTB 5,7;
EXEC adaugaRelatieTB 6,8;

EXEC adaugaConsiliu 1,'www.cjcluj.ro', 67, 1;
EXEC adaugaConsiliu 2,'www.icc.ro', 54, 9;
EXEC adaugaConsiliu 3,'www.cjsibiu.ro', 55, 4;

EXEC adaugaPresedinte 22,'Fran�ois Hollande','2012-05-15',5,'';
EXEC adaugaPresedinte 23,'Paul-Henri Spaak','1952-01-23',4,'1954-12-12';
GO

--teste functii
DECLARE @var_test VARCHAR(100);
SET @var_test = dbo.normalizeazaNume('nume_test');
PRINT @var_test;
PRINT dbo.isValidEmail('eu@yaooo.com');
PRINT dbo.isValidEmail('eu@yaooocom');
PRINT dbo.isValidEmail('');

PRINT dbo.isValidVarsta(70);
PRINT dbo.isValidVarsta(700);

PRINT dbo.validateSite('www.google.ro');
PRINT dbo.validateSite('www.googlero');

PRINT dbo.isValidNume('eug');
PRINT dbo.isValidNume('eu7g');

PRINT dbo.isValidDate('2012-12-12');
PRINT dbo.isValidDate('3012-12-12');