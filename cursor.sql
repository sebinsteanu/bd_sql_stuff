
use Romania;

SELECT *
FROM SYS.objects

SELECT O.name, OP.name
FROM SYS.objects O INNER JOIN SYS.objects OP
			ON O.parent_object_id = OP.object_id
WHERE O.type = 'PK' OR O.type = 'F'

SELECT *
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE

SELECT *
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
---------------------------------------------------------------------------------------------------------------

  /*
DECLARE @vendor_id int, @vendor_name nvarchar(50),  
    @message varchar(80), @product nvarchar(50);  
  
PRINT '-------- Vendor Products Report --------';  
  
DECLARE vendor_cursor CURSOR FOR   
	SELECT VendorID, Name  
	FROM Purchasing.Vendor  
	WHERE PreferredVendorStatus = 1  
	ORDER BY VendorID;  
  
OPEN vendor_cursor  
  
FETCH NEXT FROM vendor_cursor   
INTO @vendor_id, @vendor_name  
  
WHILE @@FETCH_STATUS = 0  
BEGIN  
    PRINT ' '  
    SELECT @message = '----- Products From Vendor: ' +   
        @vendor_name  
  
    PRINT @message  
  
    -- Declare an inner cursor based     
    -- on vendor_id from the outer cursor.  
  
    DECLARE product_cursor CURSOR FOR   
    SELECT v.Name  
    FROM Purchasing.ProductVendor pv, Production.Product v  
    WHERE pv.ProductID = v.ProductID AND  
    pv.VendorID = @vendor_id  -- Variable value from the outer cursor  
  
    OPEN product_cursor  
    FETCH NEXT FROM product_cursor INTO @product  
  
    IF @@FETCH_STATUS <> 0   
        PRINT '         <<None>>'       
  
    WHILE @@FETCH_STATUS = 0  
    BEGIN  
  
        SELECT @message = '         ' + @product  
        PRINT @message  
        FETCH NEXT FROM product_cursor INTO @product  
        END  
  
    CLOSE product_cursor  
    DEALLOCATE product_cursor  
        -- Get the next vendor.  
    FETCH NEXT FROM vendor_cursor   
    INTO @vendor_id, @vendor_name  
END   
CLOSE vendor_cursor;  
DEALLOCATE vendor_cursor;  

*/

DECLARE @name varchar(50), @name1 varchar(50), @message varchar(100);  

DECLARE Myy_cursor CURSOR  
    FOR 
		SELECT S1.name,S2.name
		FROM sys.objects S1 INNER JOIN sys.objects S2 ON S1.object_id=S2.parent_object_id
		WHERE S2.type='F' OR S2.type='PK'
OPEN Myy_cursor  

FETCH NEXT FROM Myy_cursor INTO @name,@name1 

WHILE @@FETCH_STATUS = 0  
BEGIN  
	SELECT @message=@name+'   '+@name1
	PRINT @message
	FETCH NEXT FROM Myy_cursor INTO @name,@name1 
END   
CLOSE Myy_cursor;  
DEALLOCATE Myy_cursor;


SELECT *
FROM SYS.objects

SELECT *
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE

SELECT *
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS


SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU
	INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC
	ON KCU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME



DECLARE  @id2 varchar(100), @name2 varchar(100), @message2 varchar(100), @nr_col_PK INT, @cons_PK VARCHAR(100);
DECLARE Myy_cursor CURSOR FOR 
	SELECT S1.object_id, S1.name
	FROM sys.objects S1
	WHERE S1.type = 'U' AND 2 = (SELECT COUNT(*)
								FROM sys.objects S2
								WHERE S2.parent_object_id = S1.object_id AND S2.type = 'F'
								)
OPEN Myy_cursor
FETCH NEXT FROM Myy_cursor INTO @id2,@name2 
WHILE @@FETCH_STATUS = 0 
BEGIN  
	SET @nr_col_PK = (
				SELECT COUNT(*)
				FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU
						INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC
						ON KCU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME
				WHERE @name2 = TC.TABLE_NAME AND TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
	)
	if @nr_col_PK = 2
	BEGIN
		SET @cons_PK = (
					SELECT CONSTRAINT_NAME
					FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
					WHERE CONSTRAINT_NAME = 
		);


		SELECT @message2=CAST(@id2 AS CHAR(50))+ @name2
		PRINT @message2
	END
	FETCH NEXT FROM Myy_cursor INTO @id2,@name2 
END   

CLOSE Myy_cursor;  
DEALLOCATE Myy_cursor;


/*
3.
if (dbo.varsta_e_valida(@varsta)=1)
	INSERT..
	*/


