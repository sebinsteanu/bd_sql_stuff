use examen_SGBD;

DROP TABLE PrajCofetar
DROP TABLE Cofetari
DROP TABLE Prajituri
DROP TABLE TipPrajituri
DROP TABLE Promotii

	
	
CREATE TABLE TipPrajituri
	(
	codT INT PRIMARY KEY,
	nume VARCHAR(50),
	regiune VARCHAR(50),
	)
	

CREATE TABLE Promotii
	(
	codP INT PRIMARY KEY,
	denumire VARCHAR(50),
	discount FLOAT,
	data DATE,
	)
	

CREATE TABLE Prajituri
	(
	codP INT PRIMARY KEY,
	denumire VARCHAR(50),
	gramaj FLOAT,
	pret FLOAT,
	codT INT REFERENCES TipPrajituri(codT),
	codProm INT REFERENCES Promotii(codP)
	)

CREATE TABLE Cofetari
	(
	codC INT PRIMARY KEY,
	nume VARCHAR(50),
	experienta INT,
	nrStele INT,
	codP INT REFERENCES Prajituri(codP)
	)
	
	
	
CREATE TABLE PrajCofetar
	(
		codC INT references Cofetari(codC),
		codP int references Prajituri(codP),
		primary key (codP,codC)
	)






	--transaction 1

GO


BEGIN TRAN											--dirty reads
	UPDATE TipPrajituri SET nume = 'test'
		WHERE regiune = 'Dobrogea'
	WAITFOR DELAY '00:00:07'
	ROLLBACK TRANSACTION							
GO





INSERT INTO TipPrajituri VALUES (4,'Unknown','Dobrogea'), (1,'Negresa', 'Dobrogea'),(2,'Tiramisu','Moldova'),(3,'Strudel','Muntenia')


INSERT INTO Promotii VALUES (1,'promo1',22.33, '1-1-2000'),(2,'promo2',26.36, '1-1-2017'),(3,'promo3',52.353, '1-3-2010')

INSERT INTO Prajituri VALUES (1,'Tiramisu',220.33, 12.5, 2, 1),(2,'praji2',289.33, 29.5, 1, 2),(3,'praji3',220.33, 12.5, 1, 1)


SELECT * FROM Promotii
SELECT * FROM Prajituri
SELECT * FROM TipPrajituri


