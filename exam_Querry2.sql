	--transaction 2

use examen_SGBD;



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED				--dirty reads

SET TRANSACTION ISOLATION LEVEL READ COMMITTED					--solution: BLOCARE SHARED PE OPERATIE DE CITIRE

BEGIN TRAN
	SELECT * FROM TipPrajituri WHERE regiune = 'Dobrogea'
	WAITFOR DELAY '00:00:07'
	SELECT * FROM TipPrajituri WHERE regiune = 'Dobrogea'			--citesc inainte de terminarea tranzactiei
COMMIT TRAN
GO
