

use sem_last;

DROP TABLE Utilizator;
DROP TABLE Canal;
DROP TABLE Abonament;
DROP TABLE Film;
DROP TABLE Comentariu;

CREATE TABLE Utilizator(
	codU INT PRIMARY KEY,
	nume VARCHAR(50),
	data_nasterii DATETIME
)

CREATE TABLE Canal(
	codC INT PRIMARY KEY,
	nume VARCHAR(50),
	descriere VARCHAR(50),
)

CREATE TABLE Abonament(
	codC INT REFERENCES Canal(codC),
	codU INT REFERENCES Utilizator(codU),
	data_abonament DATETIME
	PRIMARY KEY (codC,codU)
)
CREATE TABLE Film(
	codF INT PRIMARY KEY,
	url_film VARCHAR(50),
	durata INT,
	nr_vizualizari INT,
	nr_like INT,
	nr_dislike INT,
	codC INT REFERENCES Canal(codC)
)
CREATE TABLE Comentariu(
	codCom INT PRIMARY KEY,
	dataCom DATETIME,
	textCom VARCHAR(50),
	codF INT REFERENCES Film(codF)
)
ALTER TABLE Film WITH NOCHECK
ADD CHECK (nr_like+nr_dislike <= nr_vizualizari)

INSERT INTO Utilizator
VALUES(1,'Sebi','1996-05-12'),(2,'Vasi','1996-12-23'),(3,'Maria','2000-04-25');
INSERT INTO Canal
VALUES(1,'primul','descriere1'),(2,'al doilea','descriere2'),(3,'al treilea','desc3')
INSERT INTO Abonament
VALUES (3,1,'2000-02-12'),(2,1,'2009-09-03'),(1,1,'2005-05-04'),(1,2,'2010-02-02'),(2,3,'2002-09-12')
INSERT INTO Film
VALUES
(5,'sdf2_test',15,100,10,10,3),
(4,'sdf',15,100,10,10,2),
(1,'url_1',10,100,30,20,1),
(2,'url_2',10,10,3,2,1),
(3,'url_3',10,101,33,22,2)


SELECT * FROM Canal
SELECT * FROM Abonament
SELECT * FROM Film
SELECT * FROM Utilizator


go
CREATE PROCEDURE my_procedure @numeCanal VARCHAR(50)
AS
	UPDATE Film
	SET nr_like = 0, nr_dislike = 0, nr_vizualizari = 0
	WHERE codC in ( SELECT C.codC
					FROM Canal C
					WHERE C.nume = @numeCanal
					)
go

EXEC my_procedure 'al doilea'



go
CREATE FUNCTION my_function (@U VARCHAR(50),@V INT)
RETURNS TABLE 
AS RETURN
	SELECT C.nume
	FROM Canal C
	WHERE C.codC IN (SELECT F.codC
					FROM Film F
					WHERE F.nr_vizualizari > @V AND F.codC IN (SELECT A.codC
																FROM Abonament A INNER JOIN Utilizator U
																ON A.codU = U.codU
																WHERE U.nume = @U
																GROUP BY A.codC
																)
					GROUP BY F.codC
					)
go

SELECT * FROM my_function('Sebi',1);
go

DROP FUNCTION my_function;




go

DROP FUNCTION my_function2;
go
CREATE FUNCTION my_function2 (@U VARCHAR(50),@V INT)
RETURNS TABLE 
AS RETURN
	SELECT C.nume
	FROM Canal C
	WHERE C.codC IN (SELECT F.codC
					FROM Film F
					GROUP BY F.codC
					HAVING SUM(F.nr_vizualizari) > @V
					) 
			AND C.codC IN (SELECT A.codC 
							FROM Abonament A INNER JOIN Utilizator U
												ON U.codU = A.codU
							WHERE U.nume = @U
							)
go

SELECT * FROM my_function2('Sebi',1);


