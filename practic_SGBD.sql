use practic_SGBD;
go


DROP TABLE Comentarii
DROP TABLE Likeuri
DROP TABLE Pagini
DROP TABLE Categorii
DROP TABLE Postari
DROP TABLE Utilizatori



CREATE TABLE Utilizatori
	(
	codU INT PRIMARY KEY,
	nume VARCHAR(50),
	oras VARCHAR(50),
	dataNasterii DATE
	)

	
CREATE TABLE Postari
	(
	codP INT PRIMARY KEY,
	data DATE,
	text VARCHAR(50),
	nrShareuri INT,
	codU INT REFERENCES Utilizatori(codU)
	)

	CREATE TABLE Categorii
	(
		codC INT PRIMARY KEY,
		denumire  VARCHAR(50),
		descriere VARCHAR(50)
	)

	CREATE TABLE Pagini
	(
		codP INT PRIMARY KEY,
		nume VARCHAR(50),
		codC int references Categorii(codC)
	)

	create TABLE Likeuri(
		codU INT references Utilizatori(codU),
		codP int references Pagini(codP),
		dataLike DATE,
		primary key (codU,codP)
	)

	CREATE TABLE Comentarii(
		codC INT PRIMARY KEY,
		data DATE,
		text VARCHAR(50),
		eTopCom BIT,
		codP INT REFERENCES Postari(codP)
		)




	INSERT INTO Utilizatori VALUES (1,'u1','o1','1-1-2000'), (2,'u2','o1','1-1-2001'), (3,'u3','o1','1-1-2000')

	INSERT INTO Postari VALUES (1,'1-1-2017','p1',100,1), (2,'1-1-2017','p2',100,1), (3,'1-1-2017','p3',100,2)


	SELECT * FROM Postari--Utilizatori
